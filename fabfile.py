from fabric.api import local
from fabric.operations import prompt


def init_project():
    project_name = prompt("Name this project (valid python package name):")
    local("find . -type f | grep -v '/\.' | grep -v fabfile.py | xargs sed -i '' 's/PROJECT_NAME/%s/g'" % project_name)
    local('mv PROJECT_NAME %s' % project_name)


def freeze():
    local('pip freeze --local > requirements/base.txt')
