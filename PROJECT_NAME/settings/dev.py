from .base import * # NOQA

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': '../dev-db.sqlite',              # Or path to database file if using sqlite3.
            }
        }
